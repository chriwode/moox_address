## MOOX Address Management
=========

### TYPO3-Extension moox_address

This Ext. is fork from [moox_address](https://github.com/typo3-moox/moox_address).

MOOX Address Management is a backend module to manage customers and primarily used by moox_mailer (this Ext is not published by now, write to moox@dcn.de if you want a demo).

#### More Links:

Visit http://www.moox.org for more information, free download and full documentation.

Please report all issues here: https://github.com/dcngmbh/moox_core/issues

Repository: http://typo3.org/extensions/repository/view/moox_core

Watch Video: https://www.youtube.com/watch?v=3dL-VImR2x8

Sourceforge: https://sourceforge.net/projects/moox-typo3-bootstrap/

TYPO3 Forge: https://forge.typo3.org/projects/extension-moox_core/
