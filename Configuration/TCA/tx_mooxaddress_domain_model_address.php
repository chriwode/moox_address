<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address',
		'label' => 'forename',
		'label_alt' => 'surname,email',
        'label_alt_force' => 1,
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),		
		'searchFields' => 'forename,surname,company,email',		
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('moox_address') . 'Resources/Public/Icons/tx_mooxaddress_domain_model_address.gif',
		'hideTable' => FALSE
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, gender, title, forename, surname, company, department, street, zip, city, region, country, phone, mobile, fax, email, www, images, mailing_allowed',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, gender, title, forename, surname, company, department, street, zip, city, region, country, phone, mobile, fax, email, www, images, mailing_allowed,--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,hidden, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_mooxaddress_domain_model_address',
				'foreign_table_where' => 'AND tx_mooxaddress_domain_model_address.pid=###CURRENT_PID### AND tx_mooxaddress_domain_model_address.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'cruser_id' => array(
			'label' => 'cruser_id',
			'config' => array(
				'type' => 'passthrough'
			)
		),
		'pid' => array(
			'label' => 'pid',
			'config' => array(
				'type' => 'passthrough'
			)
		),
		'crdate' => array(
			'label' => 'crdate',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'tstamp' => array(
			'label' => 'tstamp',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'gender' => Array (		
			'exclude' => 1,		
			'label'	=> 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.gender',		
			'config' => Array (
				'type' => 'select',
				'items' => Array (
						Array('LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.gender.none', 0),
						Array('LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.gender.male', 1),
						Array('LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.gender.female', 2),					
				),			
				'size' => 1,	
				'maxitems' => 1,
			)
		),
		'title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.title',
			'config' => array(
				'type' => 'input',
				'size' => 10,
				'eval' => 'trim'
			),
		),
		'forename' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.forename',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'required,trim'
			),
		),
		'surname' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.surname',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim'
			),
		),
		'company' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.company',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim'
			),
		),
		'department' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.department',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim'
			),
		),
		'street' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.street',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim'
			),
		),
		'zip' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.zip',
			'config' => array(
				'type' => 'input',
				'size' => 10,
				'eval' => 'trim'
			),
		),
		'city' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.city',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim'
			),
		),
		'region' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.region',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim'
			),
		),
		'country' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.country',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim'
			),
		),
		'phone' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.phone',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim'
			),
		),
		'mobile' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.mobile',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim'
			),
		),
		'fax' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.fax',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim'
			),
		),
		'email' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.email',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim'
			),
		),
		'www' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.www',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim'
			),
		),
		'images' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.images',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'images',
				array(
					'appearance' => array(
								'headerThumbnail' => array(
									'width' => '100',
									'height' => '100',
								),
							'createNewRelationLinkTitle' => 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.add-images'
							),
					// custom configuration for displaying fields in the overlay/reference table
					// to use the imageoverlayPalette instead of the basicoverlayPalette
					'foreign_types' => array(
						'0' => array(
							'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => array(
							'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
							'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => array(
							'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => array(
							'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => array(
							'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette'
						)
					),
				),
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
			)
		),
		'mailing_allowed' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.mailing_allowed',
			'config' => array(
				'type' => 'check',
			),
		),
		'registered' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.registered',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'unregistered' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:moox_address/Resources/Private/Language/locallang_db.xlf:tx_mooxaddress_domain_model_address.unregistered',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'register_hash' => array(
			'label' => 'register_hash',
			'config' => array(
				'type' => 'passthrough'
			)
		),
		'register_tstamp' => array(
			'label' => 'register_tstamp',
			'config' => array(
				'type' => 'passthrough'
			)
		),
	),
);

?>