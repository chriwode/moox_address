<?php
namespace DCNGmbH\MooxAddress\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package moox_address
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Address extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {
	
	/**
	 * @var DateTime
	 */
	protected $crdate;

	/**
	 * @var DateTime
	 */
	protected $tstamp;
	
	/**
	 * Sichtbarkeit
	 *
	 * @var \integer	
	 */
	protected $hidden;
	
	/**
	 * Geschlecht
	 *
	 * @var \integer	
	 */
	protected $gender;
	
	/**
	 * Titel
	 *
	 * @var \string	
	 */
	protected $title;
	
	/**
	 * Vorname
	 *
	 * @var \string
	 * @validate NotEmpty
	 */
	protected $forename;
	
	/**
	 * Nachname
	 *
	 * @var \string	
	 */
	protected $surname;		
	
	/**
	 * Firma
	 *
	 * @var \string	
	 */
	protected $company;	
	
	/**
	 * Abteilung
	 *
	 * @var \string	
	 */
	protected $department;	
	
	/**
	 * Strasse
	 *
	 * @var \string	
	 */
	protected $street;

	/**
	 * Postleitzahl
	 *
	 * @var \string	
	 */
	protected $zip;	
	
	/**
	 * Stadt
	 *
	 * @var \string	
	 */
	protected $city;	
	
	/**
	 * Region
	 *
	 * @var \string	
	 */
	protected $region;
	
	/**
	 * Land
	 *
	 * @var \string	
	 */
	protected $country;
	
	/**
	 * Telefon
	 *
	 * @var \string	
	 */
	protected $phone;
	
	/**
	 * Handy
	 *
	 * @var \string	
	 */
	protected $mobile;
	
	/**
	 * Fax
	 *
	 * @var \string	
	 */
	protected $fax;
	
	/**
	 * Email
	 *
	 * @var \string	
	 */
	protected $email;
	
	/**
	 * WWW
	 *
	 * @var \string	
	 */
	protected $www;
	
	/**
	 * images to use in the gallery
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
	 * @lazy
	 */
	protected $images;
	
	/**
	 * Newsletter erlaubt
	 *
	 * @var \integer	
	 */
	protected $mailingAllowed;
	
	/**
	 * Registriert am
	 *
	 * @var \integer	
	 */
	protected $registered;
	
	/**
	 * Abgemeldet am
	 *
	 * @var \integer	
	 */
	protected $unregistered;
	
	/**
	 * register hash
	 *
	 * @var string
	 */
    protected $registerHash;
	
	/**
	 * register timestamp
	 *
	 * @var integer
	 */
    protected $registerTstamp;
	
	/**
	 * quality
	 *
	 * @var integer
	 */
	protected $quality;
	
	/**
	 * bounces
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMailer\Domain\Model\Bounce>
	 */
	protected $bounces = NULL;
	
	/**
	 * errors
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMailer\Domain\Model\Error>
	 */
	protected $errors = NULL;
		
	/**
	 * initialize object
	 */
	public function initializeObject() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}
  
	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->bounces = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->errors = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->images = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}
	
	/**
	 * Get creation date
	 *
	 * @return integer
	 */
	public function getCrdate() {
		return $this->crdate;
	}

	/**
	 * Set creation date
	 *
	 * @param integer $crdate
	 * @return void
	 */
	public function setCrdate($crdate) {
		$this->crdate = $crdate;
	}

	/**
	 * Get year of crdate
	 *
	 * @return integer
	 */
	public function getYearOfCrdate() {
		return $this->getCrdate()->format('Y');
	}

	/**
	 * Get month of crdate
	 *
	 * @return integer
	 */
	public function getMonthOfCrdate() {
		return $this->getCrdate()->format('m');
	}

	/**
	 * Get day of crdate
	 *
	 * @return integer
	 */
	public function getDayOfCrdate() {
		return (int)$this->crdate->format('d');
	}

	/**
	 * Get timestamp
	 *
	 * @return integer
	 */
	public function getTstamp() {
		return $this->tstamp;
	}

	/**
	 * Set time stamp
	 *
	 * @param integer $tstamp time stamp
	 * @return void
	 */
	public function setTstamp($tstamp) {
		$this->tstamp = $tstamp;
	}
	
	/**
	 * Returns the hidden
	 *
	 * @return \integer $hidden
	 */
	public function getHidden() {
		return $this->hidden;
	}

	/**
	 * Sets the hidden
	 *
	 * @param \integer $hidden
	 * @return void
	 */
	public function setHidden($hidden) {
		$this->hidden = $hidden;
	}
	
	/**
	 * Returns the gender
	 *
	 * @return \integer $gender
	 */
	public function getGender() {
		return $this->gender;
	}

	/**
	 * Sets the gender
	 *
	 * @param \integer $gender
	 * @return void
	 */
	public function setGender($gender) {
		$this->gender = $gender;
	}
	
	/**
	 * Returns the title
	 *
	 * @return \string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param \string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}
	
	/**
	 * Returns the forename
	 *
	 * @return \string $forename
	 */
	public function getForename() {
		return $this->forename;
	}

	/**
	 * Sets the forename
	 *
	 * @param \string $forename
	 * @return void
	 */
	public function setForename($forename) {
		$this->forename = $forename;
	}
	
	/**
	 * Returns the surname
	 *
	 * @return \string $surname
	 */
	public function getSurname() {
		return $this->surname;
	}

	/**
	 * Sets the surname
	 *
	 * @param \string $surname
	 * @return void
	 */
	public function setSurname($surname) {
		$this->surname = $surname;
	}
	
	/**
	 * Returns the company
	 *
	 * @return \string $company
	 */
	public function getCompany() {
		return $this->company;
	}

	/**
	 * Sets the company
	 *
	 * @param \string $company
	 * @return void
	 */
	public function setCompany($company) {
		$this->company = $company;
	}
	
	/**
	 * Returns the department
	 *
	 * @return \string $department
	 */
	public function getDepartment() {
		return $this->department;
	}

	/**
	 * Sets the department
	 *
	 * @param \string $department
	 * @return void
	 */
	public function setDepartment($department) {
		$this->department = $department;
	}
	
	/**
	 * Returns the street
	 *
	 * @return \string $street
	 */
	public function getStreet() {
		return $this->street;
	}

	/**
	 * Sets the street
	 *
	 * @param \string $street
	 * @return void
	 */
	public function setStreet($street) {
		$this->street = $street;
	}
	
	/**
	 * Returns the zip
	 *
	 * @return \string $zip
	 */
	public function getZip() {
		return $this->zip;
	}

	/**
	 * Sets the zip
	 *
	 * @param \string $zip
	 * @return void
	 */
	public function setZip($zip) {
		$this->zip = $zip;
	}
	
	/**
	 * Returns the city
	 *
	 * @return \string $city
	 */
	public function getCity() {
		return $this->city;
	}

	/**
	 * Sets the city
	 *
	 * @param \string $city
	 * @return void
	 */
	public function setCity($city) {
		$this->city = $city;
	}
	
	/**
	 * Returns the region
	 *
	 * @return \string $region
	 */
	public function getRegion() {
		return $this->region;
	}

	/**
	 * Sets the region
	 *
	 * @param \string $region
	 * @return void
	 */
	public function setRegion($region) {
		$this->region = $region;
	}
	
	/**
	 * Returns the country
	 *
	 * @return \string $country
	 */
	public function getCountry() {
		return $this->country;
	}

	/**
	 * Sets the country
	 *
	 * @param \string $country
	 * @return void
	 */
	public function setCountry($country) {
		$this->country = $country;
	}
	
	/**
	 * Returns the phone
	 *
	 * @return \string $phone
	 */
	public function getPhone() {
		return $this->phone;
	}

	/**
	 * Sets the phone
	 *
	 * @param \string $phone
	 * @return void
	 */
	public function setPhone($phone) {
		$this->phone = $phone;
	}
	
	/**
	 * Returns the mobile
	 *
	 * @return \string $mobile
	 */
	public function getMobile() {
		return $this->mobile;
	}

	/**
	 * Sets the mobile
	 *
	 * @param \string $mobile
	 * @return void
	 */
	public function setMobile($mobile) {
		$this->mobile = $mobile;
	}
	
	/**
	 * Returns the fax
	 *
	 * @return \string $fax
	 */
	public function getFax() {
		return $this->fax;
	}

	/**
	 * Sets the fax
	 *
	 * @param \string $fax
	 * @return void
	 */
	public function setFax($fax) {
		$this->fax = $fax;
	}
	
	/**
	 * Returns the email
	 *
	 * @return \string $email
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Sets the email
	 *
	 * @param \string $email
	 * @return void
	 */
	public function setEmail($email) {
		$this->email = $email;
	}
	
	/**
	 * Returns the www
	 *
	 * @return \string $www
	 */
	public function getWww() {
		return $this->www;
	}

	/**
	 * Sets the www
	 *
	 * @param \string $www
	 * @return void
	 */
	public function setWww($www) {
		$this->www = $www;
	}

	/**
	 * sets the Images
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $images
	 *
	 * @return void
	 */
	public function setImages($images) {
		$this->images = $images;
	}
	 
	/**
	 * get the Images
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
	 */
	public function getImages() {
		return $this->images;
	}

	/**
     * adds a image
     *
     * @param \DCNGmbH\MooxAddress\Domain\Model\FileReference $image
     *
     * @return void
     */
    public function addImage(\DCNGmbH\MooxAddress\Domain\Model\FileReference $image) {
        $this->images->attach($image);
    }	
	
	/**
     * remove a image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     *
     * @return void
     */
    public function removeImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image) {
        $this->images->detach($image);
    }	
	
	/**
	 * Returns the mailing allowed
	 *
	 * @return \boolean $mailingAllowed
	 */
	public function getMailingAllowed() {
		return $this->mailingAllowed;
	}

	/**
	 * Sets the mailing allowed
	 *
	 * @param \boolean $mailingAllowed
	 * @return void
	 */
	public function setMailingAllowed($mailingAllowed) {
		$this->mailingAllowed = $mailingAllowed;
	}
	
	/**
	 * Returns the registered
	 *
	 * @return \integer $registered
	 */
	public function getRegistered() {
		return $this->registered;
	}

	/**
	 * Sets the registered
	 *
	 * @param \integer $registered
	 * @return void
	 */
	public function setRegistered($registered) {
		$this->registered = $registered;
	}
	
	/**
	 * Returns the unregistered
	 *
	 * @return \integer $unregistered
	 */
	public function getUnregistered() {
		return $this->unregistered;
	}

	/**
	 * Sets the unregistered
	 *
	 * @param \integer $unregistered
	 * @return void
	 */
	public function setUnregistered($unregistered) {
		$this->unregistered = $unregistered;
	}
	
	/**
     * get register hash
	 *
     * @return integer $registerHash register hash
     */
    public function getRegisterHash() {
       return $this->registerHash;
    }
     
    /**
     * set register hash
	 *
     * @param integer $registerRecoveryHash register hash
	 * @return void
     */
    public function setRegisterHash($registerHash) {
        $this->registerHash = $registerHash;
    }
	
	/**
     * get register timestamp
	 *
     * @return integer $registerTstamp register timestamp
     */
    public function getRegisterTstamp() {
       return $this->registerTstamp;
    }
     
    /**
     * set register timestamp
	 *
     * @param integer $registerTstamp register timestamp
	 * @return void
     */
    public function setRegisterTstamp($registerTstamp) {
        $this->registerTstamp = $registerTstamp;
    }
	
	/**
	 * get quality
	 *
	 * @return integer $quality quality
	 */
	public function getQuality() {
	   return $this->quality;
	}
	
	/**
	 * Returns the bounces
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMailer\Domain\Model\Bounce> $bounces
	 */
	public function getBounces() {
		return $this->bounces;
	}
		
	/**
	 * Returns the errors
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMailer\Domain\Model\Error> $errors
	 */
	public function getErrors() {
		return $this->errors;
	}
}
?>