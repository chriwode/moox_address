<?php
namespace DCNGmbH\MooxAddress\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package moox_address
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class AddressRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {
	
	protected $defaultOrderings = array (	'forename' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
											'surname' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING	);	
	
	/**
	 * Finds all addresses (overwrite)
	 *	
	 * @param array $storagePids
	 * @param array $filter
	 * @return Tx_Extbase_Persistence_QueryResultInterface The addresses
	 */
	public function findAll($storagePids = array(), $filter = array()) {
		
		$query = $this->createQuery();
		
		if(is_numeric($storagePids) && $storagePids>0){
			$storagePids = array($storagePids);
		}
		
		if(!is_array($storagePids) || count($storagePids)<1){
			$storagePids = array(0);			
		}
		
		//$query->getQuerySettings()->setRespectStoragePage(true);
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);
		$query->getQuerySettings()->setStoragePageIds($storagePids);
		
		$constraints = array();
				
		$constraints[] = $query->equals('deleted', 0);
		
		if($filter['mailing']==1){
			$constraints[] = $query->equals('mailingAllowed', 1);
		} elseif($filter['mailing']==2){
			$constraints[] = $query->equals('mailingAllowed', 0);
		}
		if($filter['state']==1){
			$constraints[] = $query->equals('hidden', 0);
		} elseif($filter['state']==2){
			$constraints[] = $query->equals('hidden', 1);
		}
		$filter['query'] = trim($filter['query']);
		if($filter['query']!=""){
			$constraints[] = $query->logicalOr(																
								$query->like('forename', "%".$filter['query']."%"),
								$query->like('surname', "%".$filter['query']."%"),
								$query->like('company', "%".$filter['query']."%"),
								$query->like('department', "%".$filter['query']."%"),
								$query->like('street', "%".$filter['query']."%"),
								$query->like('zip', "%".$filter['query']."%"),
								$query->like('city', "%".$filter['query']."%"),
								$query->like('region', "%".$filter['query']."%"),
								$query->like('country', "%".$filter['query']."%"),
								$query->like('phone', "%".$filter['query']."%"),
								$query->like('mobile', "%".$filter['query']."%"),
								$query->like('fax', "%".$filter['query']."%"),
								$query->like('email', "%".$filter['query']."%"),
								$query->like('www', "%".$filter['query']."%")
							);
		}
		
		if($filter['sortDirection']=="DESC"){
			$filter['sortDirection'] = \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING;
		} else {
			$filter['sortDirection'] = \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING;
		}
		
		if($filter['sortField']=="address"){
			$query->setOrderings (Array('zip' => $filter['sortDirection'], 'city' => $filter['sortDirection'], 'street' => $filter['sortDirection']));
		} elseif($filter['sortField']=="name"){
			$query->setOrderings (Array('forename' => $filter['sortDirection'], 'surname' => $filter['sortDirection']));
		} else {
			$sortFields = explode("|",$filter['sortField']);
			if(count($sortFields)>1){
				$sortOrderings = array();
				foreach($sortFields AS $sortField){
					if($sortField=="address"){
						$sortOrderings = array_merge($sortOrderings,Array('zip' => $filter['sortDirection'], 'city' => $filter['sortDirection'], 'street' => $filter['sortDirection']));
					} elseif($filter['sortField']=="name"){
						$sortOrderings = array_merge($sortOrderings,Array('forename' => $filter['sortDirection'], 'surname' => $filter['sortDirection']));
					} else {
						$sortOrderings = array_merge($sortOrderings,Array($sortField => $filter['sortDirection']));
					}
				}
				$query->setOrderings ($sortOrderings);
			} else {
				$query->setOrderings (Array($filter['sortField'] => $filter['sortDirection']));
			}
		}
				
		return $query->matching(
			$query->logicalAnd($constraints))->execute();
	}
	
	/**
	 * Override default findByUid function to enable also the option to turn of
	 * the enableField setting
	 *
	 * @param integer $uid id of record
	 * @param boolean $respectEnableFields if set to false, hidden records are shown
	 * @return \DCNGmbH\MooxAddress\Domain\Model\Address
	 */
	public function findByUid($uid, $respectEnableFields = TRUE) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(!$respectEnableFields);

		return $query->matching(
			$query->logicalAnd(
				$query->equals('uid', $uid),
				$query->equals('deleted', 0)
			))->execute()->getFirst();
	}
	
	/**
	 * Find by email
	 *	
	 * @param array $storagePids storage pids
	 * @param string $email id of record	 
	 * @return \DCNGmbH\MooxAddress\Domain\Model\Address
	 */
	public function findByEmail($storagePids = array(), $email) {
		
		if($email!=""){
			
			$query = $this->createQuery();
			
			if(is_numeric($storagePids) && $storagePids>0){
				$storagePids = array($storagePids);
			}
			
			if(is_array($storagePids) || count($storagePids)>0){
				$query->getQuerySettings()->setStoragePageIds($storagePids);				
			}
			
			$query->getQuerySettings()->setRespectSysLanguage(FALSE);
			$query->getQuerySettings()->setIgnoreEnableFields(TRUE);
			
			return $query->matching(
				$query->logicalAnd(				
					$query->equals('email', $email),
					$query->equals('deleted', 0)
				))->execute();
		} else {
			return NULL;
		}
	}		
	
	/**
	 * Find by email
	 *	
	 * @param array $storagePids storage pids
	 * @param string $email id of record	 
	 * @return \DCNGmbH\MooxAddress\Domain\Model\Address
	 */
	public function findByEmailAndHash($storagePids = array(), $email = "", $hash = "") {
		
		if($email!="" && $hash!=""){
			
			$query = $this->createQuery();
			
			if(is_numeric($storagePids) && $storagePids>0){
				$storagePids = array($storagePids);
			}
			
			if(is_array($storagePids) || count($storagePids)>0){
				$query->getQuerySettings()->setStoragePageIds($storagePids);				
			}
			
			$query->getQuerySettings()->setRespectSysLanguage(FALSE);
			$query->getQuerySettings()->setIgnoreEnableFields(TRUE);
			
			return $query->matching(
				$query->logicalAnd(				
					$query->logicalNot(
						$query->equals('registerHash', "")
					),
					$query->equals('email', $email),					
					$query->equals('registerHash', $hash),
					$query->equals('deleted', 0)
				))->execute();
		} else {
			return NULL;
		}
	}		
	
	/**
	 * Find duplicate of given object
	 *
	 * @param \DCNGmbH\MooxAddress\Domain\Model\Address $object object	
	 * @return \DCNGmbH\MooxAddress\Domain\Model\Address
	 */
	public function findDuplicate($object = NULL) {
		
		if($object){
		
			$query = $this->createQuery();					
			$query->getQuerySettings()->setRespectSysLanguage(FALSE);
			$query->getQuerySettings()->setIgnoreEnableFields(TRUE);

			return $query->matching(
				$query->logicalAnd(
					$query->equals('gender', $object->getGender()),
					$query->equals('title', $object->getTitle()),
					$query->equals('forename', $object->getForename()),
					$query->equals('surname', $object->getSurname()),
					$query->equals('company', $object->getCompany()),
					$query->equals('department', $object->getDepartment()),
					$query->equals('street', $object->getStreet()),
					$query->equals('zip', $object->getZip()),
					$query->equals('city', $object->getCity()),
					$query->equals('region', $object->getRegion()),
					$query->equals('country', $object->getCountry()),
					$query->equals('phone', $object->getPhone()),
					$query->equals('mobile', $object->getMobile()),
					$query->equals('fax', $object->getFax()),
					$query->equals('email', $object->getEmail()),
					$query->equals('www', $object->getWww()),					
					$query->equals('deleted', 0)
				))->execute()->getFirst();
				
		} else {
			return NULL;
		}
	}
	
	/**
	 * Find addresses by pid(list)
	 *
	 * @param array $pids pids
	 * @param boolean $raw raw
	 * @return Tx_Extbase_Persistence_QueryInterface
	 */
	public function findByPids($pids = array(), $raw = false) {
				
		$pids 	= (is_array($pids))?$pids:array($pids);		
		
		$query = $this->createQuery();
			
		$query->getQuerySettings()->setStoragePageIds($pids);
		
		if($raw){
			$query->getQuerySettings()->setReturnRawQueryResult(TRUE);
		}
		
		return $query->matching(
			$query->logicalAnd(
				$query->logicalNot($query->equals('email', "")),
				$query->equals('mailingAllowed', 1)
			)
		)->execute();						
	}
}
?>