<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package moox_address
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_MooxAddress_Hooks_ItemsProcFunc {

	/**
	 * Itemsproc function to extend the selection of storage pids in flexform
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function processStoragePidSelector(array &$config,&$pObj) {
		for($i=0;$i<count($config['items']);$i++){
			if($config['items'][$i][1]!="" && $config['items'][$i][1]!="TS"){
				$config['items'][$i][0] = $config['items'][$i][0]." [PID: ".$config['items'][$i][1]."]";
			} elseif($config['items'][$i][1]=="TS"){
				$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
				$configurationManager = $objectManager->get('\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface');				
				$configuration = $configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,"MooxAddress");								
				if($configuration['persistence']['storagePid']!=""){
					$pageRepository = $objectManager->get('TYPO3\\CMS\\Frontend\\Page\\PageRepository');
					$pageInfo = $pageRepository->getPage($configuration['persistence']['storagePid']);
					$config['items'][$i][0] = $config['items'][$i][0]." ".$pageInfo['title']." [PID: ".$configuration['persistence']['storagePid']."]";
				} else {
					unset($config['items'][$i]);
				}
			}
		}
	}
	
	/**
	 * Itemsproc function to generate the selection of template category
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function processTemplateCategorySelector(array &$config,&$pObj) {
		
		$categories = \DCNGmbH\MooxAddress\Controller\TemplateController::getTemplateCategories();
		
		$config['items'] = array();
		
		foreach($categories AS $key => $category){
			$config['items'][] = array(0 => $category, 1 => $key);
		}
		
	}
	
	/**
	 * Itemsproc function to generate the selection of email templates
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function processEmailTemplateSelector(array &$config,&$pObj) {
		
		$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$templateRepository = $objectManager->get('DCNGmbH\\MooxAddress\\Domain\\Repository\\TemplateRepository');
		$templates = $templateRepository->findAll(FALSE);
		$config['items'] = array();
		
		foreach($templates AS $template){
			$config['items'][] = array(0 => $template->getTitle(), 1 => $template->getUid());
		}
		
	}
	
	/**
	 * Itemsproc function to generate the selection of switchable controller actions
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function processPi1SwitchableControllerActionsSelector(array &$config,&$pObj) {
		
		$config['items'] = array();
		
		// Get the extensions's configuration
		$extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_address']);
		
		$actions 					= array();
		$actionsRegister 			= array("register","confirmRegistration","unregister");				
		$actionsError  				= array("error");		
		
		$actionsRegisterTmp 		= array_merge($actionsRegister,$actionsError);
		
		$actionsRegister = array();
		foreach($actionsRegisterTmp AS $actionRegister){
			$actionsRegister[] = "Pi1->".$actionRegister;
		}
		$labelRegister = $GLOBALS['LANG']->sL('LLL:EXT:moox_address/Resources/Private/Language/locallang_be.xml:pi1_controller_selection.pi1_register');
		
		$config['items'][] = array(0 => $labelRegister, 1 => implode(";",$actionsRegister));
		$config['config']['default'] = implode(";",$actionsRegister);
		
	}
}