<?php
namespace DCNGmbH\MooxAddress\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package moox_address
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Pi1Controller extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
		
	/**
	 * addressRepository
	 *
	 * @var \DCNGmbH\MooxAddress\Domain\Repository\AddressRepository
	 */
	protected $addressRepository;								
	
	/**
	 * templateRepository
	 *
	 * @var \DCNGmbH\MooxAddress\Domain\Repository\TemplateRepository
	 * @inject
	 */
	protected $templateRepository;
	
	/**
	 * extConf
	 *
	 * @var boolean
	 */
	protected $extConf;		
	
	/**
	 * storagePids
	 *
	 * @var array 	
	 */
	protected $storagePids;
	
	/**
	 * initialize the controller
	 *	 
	 * @return void
	 */
	protected function initializeAction() {
		parent::initializeAction();
		
		$this->extConf 					= unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_address']);
		
		$this->initializeStorageSettings();
		
		$this->addressRepository 		= $this->objectManager->get('DCNGmbH\\MooxAddress\\Domain\\Repository\\AddressRepository');		
	}
	
	/**
	 * initialize storage settings
	 *	 
	 * @return void
	 */
	protected function initializeStorageSettings() {
				
		//fallback to current pid if no storagePid is defined
		if (version_compare(TYPO3_version, '6.0.0', '>=')) {
			$configuration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
		} else {
			$configuration = $this->configurationManager->getConfiguration(Tx_Extbase_Configuration_ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
		}
		
		if($this->settings['storagePids']!=""){
			$this->setStoragePids(explode(",",$this->settings['storagePids']));
		} else {
			$this->setStoragePids(array());
		}
		
		if(!empty($this->settings['storagePid']) && $this->settings['storagePid']!="TS"){
			if (empty($configuration['persistence']['storagePid'])) {
				$storagePids['persistence']['storagePid'] = $this->settings['storagePid'];
				$this->configurationManager->setConfiguration(array_merge($configuration, $storagePids));
			} else {
				$configuration['persistence']['storagePid'] = $this->settings['storagePid'];
				$this->configurationManager->setConfiguration($configuration);
			}
		}			
	}
		
	/**
	 * action register
	 *	
	 * @param \array $register
	 * @param \string $email
	 * @param \string $hash	 
	 * @return void
	 */
	public function registerAction($register = array(),$email = "", $hash = "") {					
		
		if($_GET['type']=="94636346"){
			$this->redirect("unregister",NULL,NULL,array('email' => $email, 'hash' => $hash));
		}
		
		$fieldnames['title'] 		= "Titel";
		$fieldnames['forename'] 	= "Vorname";
		$fieldnames['surename'] 	= "Nachname";
		$fieldnames['company'] 		= "Firma";
		$fieldnames['department']	= "Abteilung";
		$fieldnames['street'] 		= "Straße, Hausnr.";
		$fieldnames['zip'] 			= "PLZ";
		$fieldnames['city'] 		= "Stadt/Ort";
		$fieldnames['region'] 		= "Region";
		$fieldnames['country'] 		= "Land";
		$fieldnames['phone'] 		= "Telefon";
		$fieldnames['mobile'] 		= "Handy";
		$fieldnames['fax'] 			= "Fax";
		$fieldnames['www'] 			= "WWW";
		$fieldnames['email'] 		= "Email";
		
		$fields = array();
		$requiredFields = array();
		if($this->settings['requiredFields']!=""){
			$requiredFields = explode(",",$this->settings['requiredFields']);
		}
		if($this->settings['fields']!=""){
			$fieldsTmp = explode(",",$this->settings['fields']);						
			foreach($fieldsTmp AS $field){
				if(in_array($field,$requiredFields)){
					$fields[$field] = array("key" => $field, "required" => 1);
				} else {
					$fields[$field] = array("key" => $field, "required" => 0);
				}
			}
		}
		if(isset($fields['email'])){
			$fields['email']['required'] = 1;
		} else {
			$fields['email'] = array("key" => 'email', "required" => 1);
		}
		
		if(isset($register['save'])){
			
			$hasErrors 		= false;			
			$errors 		= array();
			$errorMessages 	= array();
			
			if(trim($register['email'])!="" && !\TYPO3\CMS\Core\Utility\GeneralUtility::validEmail(trim($register['email']))){					
				$errorMessages[] 		= 	array( 
													"title" => "Email",
													"message" => "Bitte geben Sie eine korrekte Email-Adresse ein."
										);					
				$allErrors['email']		= true;				
				$hasErrors 				= true;
			} elseif($this->settings['allowMailing'] && $this->addressRepository->findByEmail($this->storagePids,trim($register['email']))->count()>0){
				$errorMessages[] 		= 	array( 
													"title" => "Email",
													"message" => "Diese Email-Adresse wurde bereits registriert."
											);					
				$allErrors['email']		= true;			
				$hasErrors 				= true;
			}
			
			foreach($fields AS $field){
				if($field['required'] && trim($register[$field['key']])==""){
					$errorMessages[] 		= 	array( 
														"title" => $fieldnames[$field['key']],
														"message" => "Bitte geben Sie das Feld \"".$fieldnames[$field['key']]."\" ein."
											);					
					$allErrors[$field['key']]		= true;				
					$hasErrors 				= true;
				}
			}
							
			if(!$hasErrors){
				
				if($this->settings['allowMailing'] && trim($register['email'])!=""){
					$register['mailingAllowed'] = true;
				}
				
				$object = $this->objectManager->get('DCNGmbH\\MooxAddress\\Domain\\Model\\Address');
				
				$object->setGender($register['gender']);			
				$object->setTitle($register['title']);
				$object->setForename($register['forename']);
				$object->setSurname($register['surname']);
				$object->setCompany($register['company']);
				$object->setDepartment($register['department']);
				$object->setStreet($register['street']);
				$object->setZip($register['zip']);
				$object->setCity($register['city']);
				$object->setRegion($register['region']);
				$object->setCountry($register['country']);
				$object->setPhone($register['phone']);
				$object->setMobile($register['mobile']);
				$object->setFax($register['fax']);
				$object->setEmail($register['email']);
				$object->setWww($register['www']);
				$object->setMailingAllowed($register['mailingAllowed']);
				if($register['mailingAllowed']){
					$object->setRegistered(time());
				}
				if($this->settings['doubleOptIn']){
					$object->setHidden(true);
				}
				$object->setRegisterHash(md5($object->getEmail().time().$GLOBALS["TSFE"]->id));
				$object->setRegisterTstamp(time());
				
				$this->addressRepository->add($object);								
				$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
				
				if($this->settings['doubleOptIn']){
					
					$mailTemplate = $this->loadTemplate($this->settings['registerEmailTemplate']);
			
					if($mailTemplate['uid']>0){
						$email = array();
						$email['sendername'] 		= $this->settings['registerSendername'];
						$email['senderaddress'] 	= $this->settings['registerSenderaddress'];
						$email['receivername'] 		= $object->getSurname();
						if($object->getForename()!=""){
							$email['receivername']	= $object->getForename()." ".$email['receivername'];
						}
						$email['receiveraddress'] 	= $object->getEmail();
						$email['email'] 			= $object->getEmail();
						$email['gender'] 			= $object->getGender();
						$email['title'] 			= $object->getTitle();					
						$email['forename'] 			= $object->getForename();
						$email['surname'] 			= $object->getSurname();
						$email['name']				= $email['forename'];
						if($email['surname']!=""){
							$email['name'] .= " ".$email['surname'];
						}
						$email['subject'] 			= $this->prepareSubject($mailTemplate['subject'],$email);
						$email['body'] 				= $this->getEmailBody($mailTemplate,$email);
						$email['pid'] 				= $GLOBALS["TSFE"]->id;							
						$email['hash']				= $object->getRegisterHash();							
						$email['url'] 				= $this->uriBuilder->setTargetPageUid($email['pid'])->setNoCache(TRUE)->setCreateAbsoluteUri(TRUE)->uriFor('confirmRegistration', array("email" => $email['email'], "hash" => $email['hash']), 'Pi1', 'MooxAddress', 'Pi1');
						$email['body'] 				= $this->getEmailBody($mailTemplate,$email);
						
						$this->sendMail($email);
						
					} else {		
						$this->flashMessageContainer->add(
							"Bitte wenden Sie Sich an den Administrator", 
							"Es ist ein Fehler aufgetreten [no mail template selected]:", 
							\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR
						);			
					}
				}
				
				unset($register);
				$this->view->assign('registered', true);
				
			} else {					
				
				foreach($errorMessages AS $errorMessage){
					$this->flashMessageContainer->add($errorMessage['message'], ($errorMessage['title']!="")?$errorMessage['title'].": ":"", \TYPO3\CMS\Core\Messaging\FlashMessage::ERROR);
				}
					
				$this->view->assign('allErrors', $allErrors);												
			}			
		} 				
		
		$this->view->assign('object', $register);			
		$this->view->assign('action', 'register');
		$this->view->assign('fields', $fields);
	}
	
	/**
	 * action confirm registration
	 *	
	 * @param \string $email
	 * @param \string $hash
	 * @return void
	 */
	public function confirmRegistrationAction($email = "", $hash = "") {					
		
		if($email!="" && $hash!=""){
			$addresses = $this->addressRepository->findByEmailAndHash($this->storagePids,$email,$hash);
			if($addresses->count()==1){
				$address = $addresses->getFirst();
				if($address->getHidden()==1){
					$successMessage = "Ihre Registrierung wurde erfolgreich abgeschlossen.";
					$address->setHidden(0);
					$address->setRegisterHash(md5($address->getRegisterHash().time()));
					$this->addressRepository->update($address);								
					$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
					
					if($this->settings['registerSuccessPid']>0){
						$uri = $this->uriBuilder->setTargetPageUid($this->settings['registerSuccessPid']);						
						$this->redirectToURI($uri);
					}
					
				} else {
					$errorMessage = "Ihre Registrierung wurde bereits freigeschaltet.";
				}
			} elseif($addresses->count()>0){
				$errorMessage = "Ihre Registrierung konnte nicht eindeutig zugeordnet werden. Wenden Sie sich an den Administrator.";
			} else {
				$errorMessage = "Ihre Registrierung wurde bereits bestätigt oder konnte nicht gefunden. Wenden Sie sich gegebenenfalls an den Administrator.";
			}
			$this->view->assign('successMessage', $successMessage);
			$this->view->assign('errorMessage', $errorMessage);
		} else {
			$this->redirect("register");	
		}
	}
	
	/**
	 * action unregister
	 *	
	 * @param \string $email
	 * @param \string $hash
	 * @return void
	 */
	public function unregisterAction($email = "", $hash = "") {					
		
		if($email!="" && $hash!=""){
			$addresses = $this->addressRepository->findByEmailAndHash($this->storagePids,$email,$hash);
			if($addresses->count()==1){
				$address = $addresses->getFirst();
				if($address->getHidden()==0){
					$successMessage = "Ihre Registrierung wurde erfolgreich gelöscht.";
					if($this->settings['deleteUnregistered']){
						$this->addressRepository->remove($address);
					} else {
						if(!$this->settings['unsetMailingAllowedOnly']){
							$address->setHidden(1);	
						}							
						$address->setUnregistered(time());
						$address->setMailingAllowed(0);
						$this->addressRepository->update($address);
					}
					$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
					
					if($this->settings['unregisterSuccessPid']>0){
						$uri = $this->uriBuilder->setTargetPageUid($this->settings['unregisterSuccessPid']);						
						$this->redirectToURI($uri);
					}
					
				} else {
					$errorMessage = "Ihre Registrierung wurde bereits gelöscht.";
				}
			} elseif($addresses->count()>0){
				$errorMessage = "Ihre Registrierung konnte nicht eindeutig zugeordnet werden. Wenden Sie sich an den Administrator.";
			} else {
				$errorMessage = "Ihre Registrierung wurde bereits gelöscht oder konnte nicht gefunden. Wenden Sie sich gegebenenfalls an den Administrator.";
			}
			$this->view->assign('successMessage', $successMessage);
			$this->view->assign('errorMessage', $errorMessage);			
		} else {
			$this->redirect("register");	
		}
	}
	
	/**
	 * load template
	 *
	 * @param \int $uid
	 * @return array/object $template
	 */
	public function loadTemplate($uid = 0) {
		
		$template = array();
		
		if($uid>0){
		
			$object = $this->templateRepository->findByUid($uid);
			
			if($object->getTemplate()!=""){
				$template['uid'] 		= $object->getUid();
				$template['title'] 		= $object->getTitle();
				$template['subject'] 	= $object->getSubject();
				$template['category'] 	= $object->getCategory();
				$template['template'] 	= $object->getTemplate();				
			}
		}
		
		return $template;
	}
	
	/**
	 * get email template and render email body
	 *
	 * @param array $template template
	 * @param array $variables variables
	 * @return string $emailBody email body
	 */
	private function getEmailBody($template, $variables) {
		
		if (!empty($this->extConf['mailRenderingPartialRoot'])){
			$partialRootPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($this->extConf['mailRenderingPartialRoot']);
			if(!is_dir($partialRootPath)){
				unset($partialRootPath);	
			} 
		} 
			
		if($partialRootPath==""){
			$conf = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
			$partialRootPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(str_replace("Backend/","",$conf['view']['partialRootPath'])."Mail");
		}
		
		$emailView = $this->objectManager->create('TYPO3\\CMS\\Fluid\\View\StandaloneView');
        $emailView->setFormat('html');
        $emailView->setTemplateSource($template['template']);
		if($partialRootPath!=""){
			$emailView->setPartialRootPath($partialRootPath);
		}
        $emailView->assignMultiple($variables);
        $emailBody = $emailView->render();
		
        return $emailBody;
    }
	
	/**
	 * prepare mail subject
	 *
	 * @param string $subject subject
	 * @param array $variables $variables
	 * @return string $subject
	 */
	public function prepareSubject($subject,$variables = NULL) {
        
		$subject = str_replace("#KW#",date("W"),$subject);
		$subject = str_replace("#YEAR#",date("Y"),$subject);
		$subject = str_replace("#MONTH#",date("m"),$subject);
		$subject = str_replace("#DAY#",date("d"),$subject);
		$subject = str_replace("#TITLE#",$variables['title'],$subject);	
		$subject = str_replace("#NAME#",$variables['name'],$subject);
		$subject = str_replace("#FORENAME#",$variables['lastname'],$subject);		
		$subject = str_replace("#SURNAME#",$variables['surname'],$subject);
		$subject = str_replace("#EMAIL#",$variables['email'],$subject);
				
		return $subject;
	}
	
	/**
	 * send email
	 *
	 * @param array $mail mail
	 * @param array $variables variables
	 * @return string $emailBody email body
	 */
	private function sendMail($email) {
				
		if($this->extConf['useSMTP']){
			$TYPO3_CONF_VARS['MAIL']['transport'] 				= "smtp";
			if($this->extConf['smtpEncrypt']!="" && $this->extConf['smtpEncrypt']!="none"){
				$TYPO3_CONF_VARS['MAIL']['transport_smtp_server'] 	= $this->extConf['smtpEncrypt'];
			}
			$TYPO3_CONF_VARS['MAIL']['transport_smtp_encrypt']  = $this->extConf['smtpServer'];
			$TYPO3_CONF_VARS['MAIL']['transport_smtp_username'] = $this->extConf['smtpUsername'];
			$TYPO3_CONF_VARS['MAIL']['transport_smtp_password'] = $this->extConf['smtpPassword'];
		}
		
		if($email['sendername']==""){
			$email['sendername'] = $email['senderaddress'];
		}
		
		if($email['receivername']==""){
			$email['receivername'] = $email['receiveraddress'];
		}
		
		$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\Mail\MailMessage');				
		$mail->setFrom(array($email['senderaddress'] => $email['sendername']));
		$mail->setTo(array($email['receiveraddress'] => $email['receivername']));						
		$mail->setSubject($email['subject']);
		$mail->setBody(strip_tags($email['body']));
		$mail->addPart($email['body'], 'text/html');
		$mail->send();
	}
	
	/**
	 * action error
	 *
	 * @return void
	 */
	public function errorAction() {				
		$this->redirect("register");			
	}
	
	/**
	 * Returns storage pids
	 *
	 * @return array
	 */
	public function getStoragePids() {
		return $this->storagePids;
	}

	/**
	 * Set storage pids
	 *
	 * @param array $storagePids storage pids
	 * @return void
	 */
	public function setStoragePids($storagePids) {
		$this->storagePids = $storagePids;
	}
	
	/**
	 * Returns ext conf
	 *
	 * @return array
	 */
	public function getExtConf() {
		return $this->extConf;
	}

	/**
	 * Set ext conf
	 *
	 * @param array $extConf ext conf
	 * @return void
	 */
	public function setExtConf($extConf) {
		$this->extConf = $extConf;
	}
}
?>