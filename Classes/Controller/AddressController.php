<?php
namespace DCNGmbH\MooxAddress\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package moox_address
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class AddressController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
		
	/**
	 * addressRepository
	 *
	 * @var \DCNGmbH\MooxAddress\Domain\Repository\AddressRepository
	 */
	protected $addressRepository;				
	
	/**
	 * @var \TYPO3\CMS\Frontend\Page\PageRepository
	 */
	protected $pageRepository;
	
	/**
	 * @var \TYPO3\CMS\Core\Resource\StorageRepository
	 */
	protected $storageRepository;
	
	/**
	 * fileRepository
	 *
	 * @var \DCNGmbH\MooxAddress\Domain\Repository\FileRepository
	 */
	protected $fileRepository;
	
	/**
	 * fileReferenceRepository
	 *
	 * @var \DCNGmbH\MooxAddress\Domain\Repository\FileReferenceRepository
	 */
	protected $fileReferenceRepository;
	
	/**
	 * backend session
	 *
	 * @var \DCNGmbH\MooxAddress\Service\BackendSessionService
	 */
	protected $backendSession;
	
	/**
	 * page
	 *
	 * @var integer
	 */
	protected $page;
	
	/**
	 * extConf
	 *
	 * @var boolean
	 */
	protected $extConf;
	
	/**
	 * mailerActive
	 *
	 * @var boolean
	 */
	protected $mailerActive;
	
	/**
	 * allowedFields
	 *
	 * @var array
	 */
	protected $allowedFields;
	
	/**
	 * sort helper function
	 *
	 * @param \array $a
	 * @param \array $b
	 * @return void
	 */
	public function sortByFolderAndTitle($a, $b) {
		return strcmp($a["folder"].$a["title"], $b["folder"].$b["title"]);
	}
	
	/**
	 * initialize the controller
	 *	 
	 * @return void
	 */
	protected function initializeAction() {
		parent::initializeAction();
		
		$this->setPage((int)\TYPO3\CMS\Core\Utility\GeneralUtility::_GET('id'));
		
		$this->initializeStorageSettings();
		
		$this->setMailerActive(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('moox_mailer'));
		
		$this->setAllowedFields(
									array(	"name",
											"gender",
											"title",
											"forename",
											"surname",
											"company",
											"department",
											"address",
											"street",
											"zip",
											"city",
											"region",
											"country",
											"phone",
											"mobile",
											"fax",
											"email",
											"www"
									)
								);
		
		$this->extConf 					= unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_address']);
		$this->addressRepository 		= $this->objectManager->get('DCNGmbH\\MooxAddress\\Domain\\Repository\\AddressRepository');
		$this->storageRepository 		= $this->objectManager->get('TYPO3\\CMS\\Core\\Resource\\StorageRepository');		
		$this->fileRepository 			= $this->objectManager->get('DCNGmbH\\MooxAddress\\Domain\\Repository\\FileRepository');
		$this->fileReferenceRepository 	= $this->objectManager->get('DCNGmbH\\MooxAddress\\Domain\\Repository\\FileReferenceRepository');
		$this->pageRepository 			= $this->objectManager->get('TYPO3\\CMS\\Frontend\\Page\\PageRepository');
		$this->backendSession 			= $this->objectManager->get('DCNGmbH\\MooxAddress\\Service\\BackendSessionService');
	}
	
	/**
	 * initialize storage settings
	 *	 
	 * @return void
	 */
	protected function initializeStorageSettings() {
				
		//fallback to current pid if no storagePid is defined
		if (version_compare(TYPO3_version, '6.0.0', '>=')) {
			$configuration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
		} else {
			$configuration = $this->configurationManager->getConfiguration(Tx_Extbase_Configuration_ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
		}
		if (empty($configuration['persistence']['storagePid'])) {
			$currentPid['persistence']['storagePid'] = $this->page;
			$this->configurationManager->setConfiguration(array_merge($configuration, $currentPid));
		}		
	}
	
	/**
	 * action index
	 *
	 * @param \array $filter
	 * @return void
	 */
	public function indexAction($filter = array()) {			
		
		$folders 	= $this->getFolders();
		
		$this->redirectToFolder($folders);
		
		$filter = $this->processFilter($filter);
		
		if($this->settings['listViewFieldSeparator']!=""){
			$listViewFieldSeparator = $this->settings['listViewFieldSeparator'];
		} else {
			$listViewFieldSeparator = "&nbsp;|&nbsp;";
		}
		
		if($this->request->hasArgument('@widget_0')){
			$pagination = $this->request->getArgument('@widget_0');			
		}
		
		$rootline = $this->pageRepository->getRootLine($this->page);
		
		foreach($rootline AS $rootlinepage){
			if($rootlinepage['is_siteroot']){
				$rootpage = $rootlinepage;
				break;
			}
		}
		
		if(!$rootpage){
			$rootpage = $rootline[0];
		}
		
		$rootfound = false;
		for($i=0;$i<count($rootline);$i++){
			if($rootfound){
				unset($rootline[$i]);
			} else {
				if($rootline[$i]['is_siteroot']){
					$rootfound = true;
				}
			}
		}
		
		$rootline = array_reverse($rootline);
		
		if(isset($rootline[count($rootline)-2])){			
			$pageInfo = $this->pageRepository->getPage((int)$rootline[count($rootline)-2]['uid']);		
			if($pageInfo['module']=='mxaddress'){
				$folder = $pageInfo['uid'];				
			}
			
		}		
		
		$addresses 	= $this->addressRepository->findAll($this->page,$filter);
		
		$this->view->assign('addresses', $addresses);
		$this->view->assign('fields', $this->getListViewFields());
		$this->view->assign('fieldsSeparator', $listViewFieldSeparator);		
		$this->view->assign('filter', $filter);		
		$this->view->assign('action', 'show');
		$this->view->assign('page', $this->page);
		$this->view->assign('folder', $folder);
		$this->view->assign('rootpage', $rootpage);
		$this->view->assign('rootline', $rootline);
		$this->view->assign('folders', (count($folders)>0)?$folders:false);
		$this->view->assign('pagination', $pagination);
		$this->view->assign('replacements', $this->getQueryReplacements($filter['query']));
		$this->view->assign('mailerActive', $this->mailerActive);
	}
	
	/**
	 * action import
	 *
	 * @param \array $import
	 * @return void
	 */
	public function importAction($import = array()) {			
		
		$allowedFields = 	array( 
									"gender",
									"title",
									"forename",
									"surname",
									"company",
									"department",
									"street",
									"zip",
									"city",
									"region",
									"country",
									"phone",
									"mobile",
									"fax",
									"email",
									"www",
									"unused"								
							);
		
		if(isset($import['process'])){
			
			$csvFields = explode($import['separator'],$import['format']);
			
			$hasErrors 		= false;			
			$errors 		= array();
			$errorMessages 	= array();
			
			if(trim($import['file']['name'])==""){					
				$errorMessages[] 		= 	array( 
													"title" => "Datei",
													"message" => "Bitte wählen Sie eine Import-Datei aus"
										);					
				$allErrors['file']		= true;
				$dataError 				= true;
				$hasErrors 				= true;
			} else {
				$fileInfo = $this->getFileInfo($import['file']['name']);
				if(!in_array($fileInfo['extension'],array("csv"))){					
					$errorMessages[] 		= 	array( 
														"title" => "Datei",
														"message" => 'Es sind nur Dateien vom Typ "csv" erlaubt'									
												);					
					$allErrors['file']		= true;
					$dataError 				= true;
					$hasErrors 				= true;
				}
			}
			if(trim($import['format'])==""){					
				$errorMessages[] 		= 	array( 
													"title" => "Format",
													"message" => "Bitte geben Sie ein Import-Format vor"
										);					
				$allErrors['format']		= true;
				$dataError 				= true;
				$hasErrors 				= true;
			} elseif(trim($import['separator'])!=""){				
				if(!in_array("forename",$csvFields)){					
					$errorMessages[] 		= 	array( 
														"title" => "Format",
														"message" => 'Ihr Format muss mindestens das Feld "forename" beinhalten'
											);					
					$allErrors['format']		= true;
					$dataError 				= true;
					$hasErrors 				= true;
				}
			}
			if(trim($import['separator'])==""){					
				$errorMessages[] 		= 	array( 
													"title" => "Trennzeichen",
													"message" => "Bitte geben Sie ein Trennzeichen vor"
										);					
				$allErrors['separator']		= true;
				$dataError 				= true;
				$hasErrors 				= true;
			}
				
			if(!$hasErrors){
								
				$row 			= 1;
				$handle 		= fopen($import['file']['tmp_name'], "r");
				
				$errors 		= 0;
				$added 			= 0;
				$skipped 		= 0;
				$exisiting 		= 0;
				
				$csvFieldsTmp 	= $csvFields;
				$csvFields 		= array();
				
				$cnt 			= 0;
				foreach($csvFieldsTmp AS $csvField){
					if(in_array(strtolower($csvField),$allowedFields)){
						$csvFields[$cnt] = strtolower($csvField);
					} else {
						$csvFields[$cnt] = "unused";
					}
					$cnt++;
				}
								
				while (($data = fgetcsv($handle, 5000, $import['separator'])) !== FALSE) {
					
					$skip = false;
					
					if($import['skipFirst'] && $row==1){
						$skip = true;
					} elseif(!$import['skipFirst'] && $row==1 && in_array("forename",$data)){
						$skip = true;
					}
					
					if(!$skip){
						
						$object = $this->objectManager->get('DCNGmbH\\MooxAddress\\Domain\\Model\\Address');
						
						$row++;
						
						$num = count($data);
					    
						for ($c=0; $c < $num; $c++) {
							if($csvFields[$c] == "gender"){
								if(in_array(strtolower(trim($data[$c])),array("herr","männlich","m","mr","male","1"))){
									$data[$c] = 1;
								} elseif(in_array(strtolower(trim($data[$c])),array("frau","weiblich","w","mrs","female","f","2"))){
									$data[$c] = 2;
								} else {
									$data[$c] = 0;
								}
								$object->setGender($data[$c]);			
							} elseif($csvFields[$c] == "title"){
								$object->setTitle(trim($data[$c]));			
							} elseif($csvFields[$c] == "forename"){								
								$object->setForename(trim($data[$c]));			
							} elseif($csvFields[$c] == "surname"){
							    $object->setSurname(trim($data[$c]));
							} elseif($csvFields[$c] == "company"){
								$object->setCompany(trim($data[$c]));
							} elseif($csvFields[$c] == "department"){
								$object->setDepartment(trim($data[$c]));
							} elseif($csvFields[$c] == "street"){
								$object->setStreet(trim($data[$c]));
							} elseif($csvFields[$c] == "zip"){
								$object->setZip(trim($data[$c]));
							} elseif($csvFields[$c] == "city"){
								$object->setCity(trim($data[$c]));
							} elseif($csvFields[$c] == "region"){
								$object->setRegion(trim($data[$c]));
							} elseif($csvFields[$c] == "country"){
								$object->setCountry(trim($data[$c]));
							} elseif($csvFields[$c] == "phone"){
								$object->setPhone(trim($data[$c]));
							} elseif($csvFields[$c] == "mobile"){
								$object->setMobile(trim($data[$c]));
							} elseif($csvFields[$c] == "fax"){
								$object->setFax(trim($data[$c]));
							} elseif($csvFields[$c] == "email"){
								$object->setEmail(trim($data[$c]));
							} elseif($csvFields[$c] == "www"){
								$object->setWww(trim($data[$c]));
							}
						}
						
						$isValid = true;
						
						if($object->getGender()==""){
							$object->setGender(0);
						}						
						if($object->getForename()==""){
							$isValid = false;
						}
						if($object->getEmail()!="" && !\TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($object->getEmail())){
							$isValid = false;
						}
						if($import['mailingAllowed'] && $object->getEmail()!=""){
							$object->setMailingAllowed(1);
							$object->setRegistered(time());
						}
						if($isValid){
							$duplicate = $this->addressRepository->findDuplicate($object);
							if(!is_object($duplicate)){
								if($import['mode']>0){
									$foundAddress = $this->addressRepository->findByEmail(NULL,$object->getEmail())->getFirst();									
									if(!is_object($foundAddress)){
										$this->addressRepository->add($object);										
										$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
										$added++;
									} elseif(is_object($foundAddress) && in_array($import['mode'],array(1,2))){
										if($import['mode']==2 && $foundAddress->getUnregistered()==0){
											$foundAddress->setMailingAllowed($object->getMailingAllowed());
											$foundAddress->setRegistered($object->getRegistered());
										}
										if($import['mode']==2 || $foundAddress->getGender()==0){
											$foundAddress->setGender($object->getGender());
										}
										if($import['mode']==2 || $foundAddress->getTitle()==""){
											$foundAddress->setTitle($object->getTitle());
										}
										if($import['mode']==2 || $foundAddress->getForename()==""){
											$foundAddress->setForename($object->getForename());
										}
										if($import['mode']==2 || $foundAddress->getSurname()==""){
											$foundAddress->setSurname($object->getSurname());
										}
										if($import['mode']==2 || $foundAddress->getCompany()==""){
											$foundAddress->setCompany($object->getCompany());
										}
										if($import['mode']==2 || $foundAddress->getDepartment()==""){
											$foundAddress->setDepartment($object->getDepartment());
										}
										if($import['mode']==2 || $foundAddress->getStreet()==""){
											$foundAddress->setStreet($object->getStreet());
										}
										if($import['mode']==2 || $foundAddress->getZip()==""){
											$foundAddress->setZip($object->getZip());
										}
										if($import['mode']==2 || $foundAddress->getCity()==""){
											$foundAddress->setCity($object->getCity());
										}
										if($import['mode']==2 || $foundAddress->getRegion()==""){
											$foundAddress->setRegion($object->getRegion());
										}
										if($import['mode']==2 || $foundAddress->getCountry()==""){
											$foundAddress->setCountry($object->getCountry());
										}
										if($import['mode']==2 || $foundAddress->getPhone()==""){
											$foundAddress->setPhone($object->getPhone());
										}
										if($import['mode']==2 || $foundAddress->getMobile()==""){
											$foundAddress->setMobile($object->getMobile());
										}
										if($import['mode']==2 || $foundAddress->getFax()==""){
											$foundAddress->setFax($object->getFax());
										}
										if($import['mode']==2 || $foundAddress->getWww()==""){
											$foundAddress->setWww($object->getWww());
										}
										$this->addressRepository->update($foundAddress);
										$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
										$updated++;
									} else {
										$skipped++;
									}
								} else {
									$this->addressRepository->add($object);
									$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
									$added++;
								}																									
							} else {
								$exisiting++;
							}
						} else {
							$errors++;
						}

						$object = NULL;
					
					} else {
						$row++;
					}
				}
				$successMessage = "";
				$infoMessage 	= "";
				$errorMessage 	= "";
				if($added>0){
					$successMessage .= $added.' Adresse(n) wurde importiert.';
				}
				if($updated>0){
					$successMessage .= ' '.$updated.' Adresse(n) wurden '.(($import['mode']==2)?"überschrieben":"upgedated").'.';
				}
				if($skipped>0){
					$infoMessage .= ' '.$skipped.' Adresse(n) wurde übersprungen.';
				}
				if($exisiting>0){
					$infoMessage .= ' '.$exisiting.' Adresse(n) waren bereits vorhanden.';
				}
				if($errors>0){
					$errorMessage .= ' '.$errors.' Adresse(n) konnten wegen fehlerhafter Daten nicht importiert werden.';
				}
				
				if($successMessage!=""){
					$this->flashMessageContainer->add(
						'', 
						$successMessage, 
						\TYPO3\CMS\Core\Messaging\FlashMessage::OK);
				}
				if($infoMessage!=""){
					$this->flashMessageContainer->add(
						'', 
						$infoMessage, 
						\TYPO3\CMS\Core\Messaging\FlashMessage::INFO);
				}
				if($errorMessage!=""){
					$this->flashMessageContainer->add(
						'', 
						$errorMessage, 
						\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR);
				}
				
				if($added>0 || $updated>0){
					$this->redirect("index");
				}
				
			} else {					
					
				foreach($errorMessages AS $errorMessage){
					$this->flashMessageContainer->add($errorMessage['message'], ($errorMessage['title']!="")?$errorMessage['title'].": ":"", \TYPO3\CMS\Core\Messaging\FlashMessage::ERROR);
				}
					
				$this->view->assign('allErrors', $allErrors);					
					
				if($dataError){
					$this->view->assign('dataError',1);
				}											
			}
			
		} else {
			
			if(!count($import)){
				$import['format'] 			= ($this->settings['importFormat'])?$this->settings['importFormat']:"forename;surname;email";				
				$import['separator'] 		= ($this->settings['importSeparator'])?$this->settings['importSeparator']:";";
				$import['mode'] 			= (in_array($this->settings['importMode'],array(0,1,2,3)))?$this->settings['importMode']:"2";
				$import['skipFirst'] 		= ($this->settings['importSkipFirst'])?$this->settings['importSkipFirst']:0;
				$import['mailingAllowed'] 	= ($this->settings['importMailingAllowed'])?$this->settings['importMailingAllowed']:0;
			}
		}
		
		$rootline = $this->pageRepository->getRootLine($this->page);
		
		foreach($rootline AS $rootlinepage){
			if($rootlinepage['is_siteroot']){
				$rootpage = $rootlinepage;
				break;
			}
		}
		
		if(!$rootpage){
			$rootpage = $rootline[0];
		}
		
		$rootline = array_reverse($rootline);
		
		if(isset($rootline[count($rootline)-2])){			
			$pageInfo = $this->pageRepository->getPage((int)$rootline[count($rootline)-2]['uid']);		
			if($pageInfo['module']=='mxaddress'){
				$folder = $pageInfo['uid'];				
			}
			
		}
		
		$folders 	= $this->getFolders();
				
		$this->view->assign('action', 'import');
		$this->view->assign('page', $this->page);
		$this->view->assign('folder', $folder);
		$this->view->assign('rootpage', $rootpage);
		$this->view->assign('rootline', $rootline);
		$this->view->assign('folders', (count($folders)>0)?$folders:false);
		$this->view->assign('object', $import);
		$this->view->assign('mailerActive', $this->mailerActive);
		
	}
	
	/**
	 * action add
	 *	
	 * @param \array $add	
	 * @return void
	 */
	public function addAction($add = array()) {			
		
		$images_tmp = array();
		
		if(count($add)){
			foreach($add AS $key => $value){
				if(strpos($key,"image_")!==FALSE){					
					$imageIndex 						= str_replace("image_","",$key);
					if(!isset($add["removeImage_".$imageIndex ])){
						$imageObject 						= unserialize((string)$value);	
						$imageInfo 							= $this->getFileInfo($imageObject['name']);			
						$imagesCnt 							= count($images_tmp);
						$images_tmp[$imagesCnt]['name'] 	= $imageInfo['name'].".".$imageInfo['extension'];
						$images_tmp[$imagesCnt]['src'] 		= "/typo3temp/moox_address/".md5($imageInfo['name']).".".$imageInfo['extension'];
						$images_tmp[$imagesCnt]['object'] 	= $value;
					}
				}
			}
		} else {		
			$add['mailingAllowed'] 		= ($this->settings['addMailingAllowed'])?1:0;		
		}
		
		if($add['image']['name']!=""){
			
			$imageInfo = $this->getFileInfo($add['image']['name']);
				
			if(!in_array($imageInfo['extension'],array("png","jpg","gif"))){														
				$allErrors['image']		= true;
				if($add['addImage']){
					$this->flashMessageContainer->add(
						'Es sind nur Bilder vom Typ; "png","jpg" und "gif" erlaubt', 
						'Bild', 
						\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR);
				}
			} else {
				$imagesCnt = count($images_tmp);
				$images_tmp[$imagesCnt]['name'] 	= $imageInfo['name'].".".$imageInfo['extension'];
				$add['image']['tmp_name'] 			= $this->copyToTypo3Temp($add['image']['tmp_name'],$imageInfo['name'].".".$imageInfo['extension']);
				$images_tmp[$imagesCnt]['src'] 		= "/typo3temp/moox_address/".md5($imageInfo['name']).".".$imageInfo['extension'];
				$images_tmp[$imagesCnt]['object'] 	= serialize($add['image']);				
			}
		}
				
		if(count($images_tmp)>0){
			$add['images'] = $images_tmp;
		}
	
		if(isset($add['save']) || isset($add['saveAndClose']) ||  isset($add['saveAndNew'])){
			
			$hasErrors 		= false;			
			$errors 		= array();
			$errorMessages 	= array();
			
			if(trim($add['email'])!="" && !\TYPO3\CMS\Core\Utility\GeneralUtility::validEmail(trim($add['email']))){					
				$errorMessages[] 		= 	array( 
													"title" => "Email",
													"message" => "Bitte geben Sie eine korrekte Email-Adresse ein."
										);					
				$allErrors['email']		= true;
				$dataError 				= true;
				$hasErrors 				= true;
			}
			if((trim($add['email'])=="" || $allErrors['email']) && $add['mailingAllowed']){
				$errorMessages[] 				= 	array( 
															"title" => "Newsletter",
															"message" => "Sie können den Empfang von Newslettern nur erlauben, wenn eine korrekte Email-Adresse eingegeben wurde."
												);					
				$allErrors['mailingAllowed']	= true;
				$dataError 						= true;
				$hasErrors 						= true;
			}
			
			if($allErrors['image']){
				$errorMessages[] 		= 	array( 
												"title" => "Bild",
												"message" => 'Es sind nur Bilder vom Typ; "png","jpg" und "gif" erlaubt'									
											);									
				$dataError 				= true;
				$hasErrors 				= true;
			}
							
			if(!$hasErrors){
			
				$object = $this->objectManager->get('DCNGmbH\\MooxAddress\\Domain\\Model\\Address');
				
				$object->setGender($add['gender']);			
				$object->setTitle($add['title']);
				$object->setForename($add['forename']);
				$object->setSurname($add['surname']);
				$object->setCompany($add['company']);
				$object->setDepartment($add['department']);
				$object->setStreet($add['street']);
				$object->setZip($add['zip']);
				$object->setCity($add['city']);
				$object->setRegion($add['region']);
				$object->setCountry($add['country']);
				$object->setPhone($add['phone']);
				$object->setMobile($add['mobile']);
				$object->setFax($add['fax']);
				$object->setEmail($add['email']);
				$object->setWww($add['www']);
				$object->setMailingAllowed($add['mailingAllowed']);
				if($add['mailingAllowed']){
					$object->setRegistered(time());
				}
				
				if(count($images_tmp)){					
					/** @var \TYPO3\CMS\Core\Resource\ResourceStorage $storage */
					$storage = $this->storageRepository->findByUid('1');
					
					foreach($images_tmp AS $image){
						$image = unserialize($image['object']);
						// this will already handle the moving of the file to the storage:
						$newFileObject = $storage->addFile(
							$image['tmp_name'], $storage->getRootLevelFolder(), $image['name']
						);
						$newFileObject = $storage->getFile($newFileObject->getIdentifier());
						$newFile = $this->fileRepository->findByUid($newFileObject->getProperty('uid'));
						
						/** @var \DCNGmbH\MooxAddress\Domain\Model\FileReference $newFileReference */
						$newFileReference = $this->objectManager->get('DCNGmbH\MooxAddress\Domain\Model\FileReference');
						$newFileReference->setFile($newFile);
						$newFileReference->setCruserId($GLOBALS['BE_USER']->user['uid']);						
						
						$object->addImage($newFileReference);
					}
				}
				
				$this->addressRepository->add($object);								
				$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
				
				$this->flashMessageContainer->add(
					'', 
					'Adresse wurde erfolgreich gespeichert.', 
					\TYPO3\CMS\Core\Messaging\FlashMessage::OK);
			
				if(isset($add['save'])){
					$this->redirect("edit",NULL,NULL,array('uid' => $object->getUid()));
				} elseif(isset($add['saveAndClose'])){
					$this->redirect("index");
				} elseif(isset($add['saveAndNew'])){			
					$this->redirect("add");
				} else {			
					$this->view->assign('object', $add);			
					$this->view->assign('action', 'add');
					$this->view->assign('page', $this->page);
					$this->view->assign('mailerActive', $this->mailerActive);
				}
				
			} else {					
					
				foreach($errorMessages AS $errorMessage){
					$this->flashMessageContainer->add($errorMessage['message'], ($errorMessage['title']!="")?$errorMessage['title'].": ":"", \TYPO3\CMS\Core\Messaging\FlashMessage::ERROR);
				}
					
				$this->view->assign('allErrors', $allErrors);					
					
				if($dataError){
					$this->view->assign('dataError',1);
				}
				
				$this->view->assign('object', $add);			
				$this->view->assign('action', 'add');
				$this->view->assign('page', $this->page);
				$this->view->assign('mailerActive', $this->mailerActive);				
			}
			
		} else {
		
			$this->view->assign('object', $add);			
			$this->view->assign('action', 'add');
			$this->view->assign('page', $this->page);
			$this->view->assign('mailerActive', $this->mailerActive);
			if($add['addImage']){
				$this->view->assign('allErrors', $allErrors);
			}
		}
	}
	
	/**
	 * action edit
	 *
	 * @param \int $uid
	 * @param \array $edit
	 * @param \array $pagination
	 * @return void
	 */
	public function editAction($uid = 0, $edit = array(), $pagination = array()) {			
		
		if(!is_null($this->backendSession->get("id")) && $this->backendSession->get("id")!=""){
			$this->setPage($this->backendSession->get("id"));
		}
		
		if($uid>0){						
			
			$object = $this->addressRepository->findByUid($uid,FALSE);
			
			if(count($edit)){
				foreach($edit AS $key => $value){
					if(strpos($key,"removeImage_")!==FALSE){					
						$referenceUid 	= str_replace("removeImage_","",$key);						
						foreach($object->getImages() AS $reference){
							$imageName = $reference->getOriginalResource()->getName();
							if($reference->getUid()==$referenceUid){
								$object = $this->addressRepository->findByUid($uid,FALSE);
								$object->removeImage($reference);
								$this->addressRepository->update($object);
								$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
								$this->flashMessageContainer->add(
									'', 
									'Bild "'.$imageName.'" wurde erfolgreich entfernt.', 
									\TYPO3\CMS\Core\Messaging\FlashMessage::OK);
							}
						}						
					}
				}
			}												
			
			if($edit['addImage'] && $edit['image']['name']!=""){
			
				$imageInfo = $this->getFileInfo($edit['image']['name']);
							
				if(!in_array($imageInfo['extension'],array("png","jpg","gif"))){														
					$this->flashMessageContainer->add(
						'Es sind nur Bilder vom Typ; "png","jpg" und "gif" erlaubt', 
						'Bild', 
						\TYPO3\CMS\Core\Messaging\FlashMessage::ERROR);								
				} else {
					/** @var \TYPO3\CMS\Core\Resource\ResourceStorage $storage */
					$storage = $this->storageRepository->findByUid('1');
					$newFileObject = $storage->addFile(
						$edit['image']['tmp_name'], $storage->getRootLevelFolder(), $edit['image']['name']
					);
					$newFileObject = $storage->getFile($newFileObject->getIdentifier());
					$newFile = $this->fileRepository->findByUid($newFileObject->getProperty('uid'));
						
					/** @var \DCNGmbH\MooxAddress\Domain\Model\FileReference $newFileReference */
					$newFileReference = $this->objectManager->get('DCNGmbH\MooxAddress\Domain\Model\FileReference');
					$newFileReference->setFile($newFile);
					$newFileReference->setCruserId($GLOBALS['BE_USER']->user['uid']);						
					
					$object->addImage($newFileReference);
					
					$this->addressRepository->update($object);
					$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
					$this->flashMessageContainer->add(
						'', 
						'Bild "'.$edit['image']['name'].'" wurde erfolgreich hinzugefügt.', 
						\TYPO3\CMS\Core\Messaging\FlashMessage::OK);
				}
			}
					
			$object = $this->addressRepository->findByUid($uid,FALSE);
			
			if(!count($edit)){
				$edit['gender'] 		= $object->getGender();
				$edit['title'] 			= $object->getTitle();
				$edit['forename'] 		= $object->getForename();
				$edit['surname'] 		= $object->getSurname();
				$edit['company'] 		= $object->getCompany();
				$edit['department']	 	= $object->getDepartment();
				$edit['street'] 		= $object->getStreet();
				$edit['zip'] 			= $object->getZip();
				$edit['city'] 			= $object->getCity();
				$edit['region'] 		= $object->getRegion();
				$edit['country'] 		= $object->getCountry();
				$edit['phone'] 			= $object->getPhone();
				$edit['mobile'] 		= $object->getMobile();
				$edit['fax'] 			= $object->getFax();
				$edit['email'] 			= $object->getEmail();
				$edit['www'] 			= $object->getWww();				
				$edit['mailingAllowed'] = $object->getMailingAllowed();				
				$edit['uid'] 			= $object->getUid();				
			}
			
			$edit['images'] 		= $object->getImages();					
						
			if(isset($edit['save']) || isset($edit['saveAndClose']) ||  isset($edit['saveAndNew'])){
				
				$hasErrors 		= false;			
				$errors 		= array();
				$errorMessages 	= array();
				
				if(trim($edit['email'])!="" && !\TYPO3\CMS\Core\Utility\GeneralUtility::validEmail(trim($edit['email']))){					
					$errorMessages[] 		= 	array( 
														"title" => "Email",
														"message" => "Bitte geben Sie eine korrekte Email-Adresse ein."
											);					
					$allErrors['email']		= true;
					$dataError 				= true;
					$hasErrors 				= true;
				}
				if((trim($edit['email'])=="" || $allErrors['email']) && $edit['mailingAllowed']){
					$errorMessages[] 				= 	array( 
																"title" => "Newsletter",
																"message" => "Sie können den Empfang von Newslettern nur erlauben, wenn eine Email-Adresse eingegeben wurde."
													);					
					$allErrors['mailingAllowed']	= true;
					$dataError 						= true;
					$hasErrors 						= true;
				}
				if($edit['image']['name']!=""){
					$imageInfo = $this->getFileInfo($edit['image']['name']);
							
					if(!in_array($imageInfo['extension'],array("png","jpg","gif"))){														
						$errorMessages[] 				= 	array( 
																	"title" => "Bild",
																	"message" => 'Es sind nur Bilder vom Typ; "png","jpg" und "gif" erlaubt'
														);					
						$allErrors['image']	= true;
						$dataError 						= true;
						$hasErrors 						= true;								
					}
				}
				
				if(!$hasErrors){					
				
					$object->setGender($edit['gender']);			
					$object->setTitle($edit['title']);
					$object->setForename($edit['forename']);
					$object->setSurname($edit['surname']);
					$object->setCompany($edit['company']);
					$object->setDepartment($edit['department']);
					$object->setStreet($edit['street']);
					$object->setZip($edit['zip']);
					$object->setCity($edit['city']);
					$object->setRegion($edit['region']);
					$object->setCountry($edit['country']);
					$object->setPhone($edit['phone']);
					$object->setMobile($edit['mobile']);
					$object->setFax($edit['fax']);
					$object->setEmail($edit['email']);
					$object->setWww($edit['www']);
					
					if($edit['image']['name']!=""){
				
						/** @var \TYPO3\CMS\Core\Resource\ResourceStorage $storage */
						$storage = $this->storageRepository->findByUid('1');
						$newFileObject = $storage->addFile(
							$edit['image']['tmp_name'], $storage->getRootLevelFolder(), $edit['image']['name']
						);
						$newFileObject = $storage->getFile($newFileObject->getIdentifier());
						$newFile = $this->fileRepository->findByUid($newFileObject->getProperty('uid'));
							
						/** @var \DCNGmbH\MooxAddress\Domain\Model\FileReference $newFileReference */
						$newFileReference = $this->objectManager->get('DCNGmbH\MooxAddress\Domain\Model\FileReference');
						$newFileReference->setFile($newFile);
						$newFileReference->setCruserId($GLOBALS['BE_USER']->user['uid']);						
							
						$object->addImage($newFileReference);
					}
					
					if($edit['mailingAllowed'] && !$object->getMailingAllowed()){						
						$object->setRegistered(time());
						$object->setUnregistered(0);
					} elseif(!$edit['mailingAllowed'] && $object->getMailingAllowed()){												
						$object->setUnregistered(time());
					} 
					$object->setMailingAllowed($edit['mailingAllowed']);
					
					$this->addressRepository->update($object);								
					$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
					
					$this->flashMessageContainer->add(
						'', 
						'Änderungen wurden erfolgreich gespeichert.', 
						\TYPO3\CMS\Core\Messaging\FlashMessage::OK);
				
					if(isset($edit['saveAndClose'])){
						if($pagination['currentPage']>1){
							$this->redirect("index","Address","MooxAddress",array('@widget_0' => $pagination));
						} else {
							$this->redirect("index");
						}
					} elseif(isset($edit['saveAndNew'])){
						$this->redirect("add");
					} else {
						$this->view->assign('object', $edit);				
						$this->view->assign('action', 'edit');
						$this->view->assign('uid', $uid);
						$this->view->assign('pagination', $pagination);
						$this->view->assign('page', $this->page);
						$this->view->assign('mailerActive', $this->mailerActive);
					}
					
				} else {					
					
					foreach($errorMessages AS $errorMessage){
						$this->flashMessageContainer->add($errorMessage['message'], ($errorMessage['title']!="")?$errorMessage['title'].": ":"", \TYPO3\CMS\Core\Messaging\FlashMessage::ERROR);
					}
					
					$this->view->assign('allErrors', $allErrors);					
					
					if($dataError){
						$this->view->assign('dataError',1);
					}
					
					$this->view->assign('object', $edit);				
					$this->view->assign('action', 'edit');
					$this->view->assign('uid', $uid);
					$this->view->assign('pagination', $pagination);
					$this->view->assign('page', $this->page);
					$this->view->assign('mailerActive', $this->mailerActive);
				}
				
			} else {
				
				$this->view->assign('object', $edit);				
				$this->view->assign('action', 'edit');
				$this->view->assign('uid', $uid);
				$this->view->assign('pagination', $pagination);
				$this->view->assign('page', $this->page);
				$this->view->assign('mailerActive', $this->mailerActive);
				if($edit['addImage']){
					$this->view->assign('allErrors', $allErrors);
				}
			}
			
		} else {
			if($pagination['currentPage']>1){
				$this->redirect("index","Address","MooxAddress",array('@widget_0' => $pagination));
			} else {
				$this->redirect("index");
			}
		}
	}
	
	/**
	 * action delete
	 *	
	 * @param \int $uid
	 * @param \array $pagination
	 * @return void
	 */
	public function deleteAction($uid = 0, $pagination = array()) {			
		
		if($uid>0){
		
			$object = $this->addressRepository->findByUid($uid,FALSE);
			
			$this->addressRepository->remove($object);
			
			$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
			
			$this->flashMessageContainer->add(
					'', 
					'Adresse wurde gelöscht.', 
					\TYPO3\CMS\Core\Messaging\FlashMessage::OK);
						
		} 
		
		if($pagination['currentPage']>1){
			$this->redirect("index","Address","MooxAddress",array('@widget_0' => $pagination));
		} else {
			$this->redirect("index");
		}
	}
	
	/**
	 * action toggle state
	 *
	 * @param \int $uid	
	 * @param \array $pagination
	 * @return void
	 */
	public function toggleStateAction($uid = 0, $pagination = array()) {			
		
		if($uid>0){						
			
			$object = $this->addressRepository->findByUid($uid,FALSE);
			
			if($object->getHidden()==1){
				$object->setHidden(0);
				$action = "aktiviert";
			} else {
				$object->setHidden(1);
				$action = "deaktiviert";
			}			
			
			$this->addressRepository->update($object);								
			$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
			
			$this->flashMessageContainer->add(
					'', 
					'Adresse wurde erfolgreich '.$action, 
					\TYPO3\CMS\Core\Messaging\FlashMessage::OK);						
		} 		
		if($pagination['currentPage']>1){			
			$this->redirect("index","Address","MooxAddress",array('@widget_0' => $pagination));			
		} else {
			$this->redirect("index");
		}
	}
	
	/**
	 * action toggle mailing allowed
	 *
	 * @param \int $uid	
	 * @param \array $pagination
	 * @return void
	 */
	public function toggleMailingAllowedAction($uid = 0, $pagination = array()) {			
		
		if($uid>0){						
			
			$object = $this->addressRepository->findByUid($uid,FALSE);
			
			if($object->getMailingAllowed()==1){
				$object->setMailingAllowed(0);
				$action = "deaktiviert";
			} else {
				$object->setMailingAllowed(1);
				$action = "aktiviert";
			}			
			
			$this->addressRepository->update($object);								
			$this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface')->persistAll();
			
			$this->flashMessageContainer->add(
					'', 
					'Newsletter-Empfang für Adresse wurde erfolgreich '.$action, 
					\TYPO3\CMS\Core\Messaging\FlashMessage::OK);						
		} 		
		if($pagination['currentPage']>1){			
			$this->redirect("index","Address","MooxAddress",array('@widget_0' => $pagination));			
		} else {
			$this->redirect("index");
		}
	}
	
	/**
	 * get address
	 *
	 * @param \int $uid
	 * @return array/object $address
	 */
	public function getAddress($uid) {
		
		$address = array();
		
		if($uid){
					
			$address = $this->addressRepository->findByUid($uid,FALSE);
		}
		
		return $address;
	}

	/**
	 * Get array of folders with addresses module	
	 *	
	 * @return	array	folders with addresses module	
	 */
	public function getFolders() {
		
		global $BE_USER;
		
		$folders = array();
		
		$query = array(
			'SELECT' => '*',
			'FROM' => 'pages',
			'WHERE' => $BE_USER->getPagePermsClause(1)." AND deleted=0 AND doktype=254 AND module='mxaddress'"
		);
		$pages = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($query);
		
		$folderCnt = 0;
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($pages)) {
			$folders[$folderCnt] = $row;
			$rootline = $this->pageRepository->getRootLine($row['uid']);
			
			foreach($rootline AS $rootlinepage){
				if($rootlinepage['is_siteroot']){
					$folders[$folderCnt]['rootpage'] = $rootlinepage;
					break;
				}
			}
			
			if(!$folders[$folderCnt]['rootpage']){
				$folders[$folderCnt]['rootpage'] = $rootline[0];
			}
					
			$rootline = array_reverse($rootline);
			
			if(isset($rootline[count($rootline)-2])){			
				$pageInfo = $this->pageRepository->getPage((int)$rootline[count($rootline)-2]['uid']);		
				if($pageInfo['module']=='mxaddress'){
					$folders[$folderCnt]['folder'] = $pageInfo['uid'];				
				}
				
			}
			
			$folders[$folderCnt]['rootline'] = $rootline;
			$folderCnt++;
		}
				
		usort($folders, array("\DCNGmbH\MooxAddress\Controller\AddressController", "sortByFolderAndTitle"));
		
		$folders = array_reverse($folders);
		
		return $folders;		
	}
	
	/**
	 * Get query replacements	
	 *
	 * @param string $query
	 * @return	array	list view fields 	
	 */
	public function getQueryReplacements($query = "") {
		
		$replacements = array();
		
		if($query!=""){
			$replacements[] = array("search" => $query, "replace" => '<span class="query">'.$query.'</span>');
			$replacements[] = array("search" => strtolower($query), "replace" => '<span class="query">'.strtolower($query).'</span>');
			$replacements[] = array("search" => strtoupper($query), "replace" => '<span class="query">'.strtoupper($query).'</span>');
			$replacements[] = array("search" => ucfirst($query), "replace" => '<span class="query">'.ucfirst($query).'</span>');
			$replacements[] = array("search" => '<span class="query"><span class="query">', "replace" => '<span class="query">');
			$replacements[] = array("search" => '</span></span>', "replace" => '</span>');
		}
		
		return $replacements;
	}
	
	/**
	 * Get list view fields	
	 *	
	 * @return	array	list view fields 	
	 */
	public function getListViewFields() {
		
		$listViewFields = array();
		if($this->settings['listViewFields']!=""){
			$listViewFieldsTmp = explode(",",$this->settings['listViewFields']);
			foreach($listViewFieldsTmp AS $listViewField){
				$listViewField = explode(":",$listViewField);
				if(in_array(trim($listViewField[0]),$this->getAllowedFields())){
					$listViewFields[] = array(	
												"name" 		=> trim($listViewField[0]),
												"lenght" 	=> (trim($listViewField[1])>0)?trim($listViewField[1]):10000
										);
				} else {
					$additionalFieldsAccepted = true;
					$listViewAdditionalFields = array();
					$listViewAdditionalFieldsTmp = explode("|",$listViewField[0]);					
					if(count($listViewAdditionalFieldsTmp)>1){
						foreach($listViewAdditionalFieldsTmp AS $listViewAdditionalField){
							if(in_array(trim($listViewAdditionalField),$this->getAllowedFields())){
								$listViewAdditionalFields[] = trim($listViewAdditionalField);									
							} else {
								$additionalFieldsAccepted = false;
								break;
							}							
						}
					} else {
						$additionalFieldsAccepted = false;
					}
					if($additionalFieldsAccepted){
						$mainViewField = $listViewAdditionalFields[0];
						unset($listViewAdditionalFields[0]);
						$listViewFields[] = array(	
													"name" 			=> $mainViewField,
													"lenght" 		=> (trim($listViewField[1])>0)?trim($listViewField[1]):10000,
													"additional" 	=> $listViewAdditionalFields
											);
					}
				}
			}
		}
		if(!count($listViewFields) && $this->extConf['listViewFields']!=""){
			$listViewFieldsTmp = explode(",",$this->extConf['listViewFields']);			
			foreach($listViewFieldsTmp AS $listViewField){
				$listViewField = explode(":",$listViewField);				
				if(in_array(trim($listViewField[0]),$this->getAllowedFields())){
					$listViewFields[] = array(	
												"name" 		=> trim($listViewField[0]),
												"lenght" 	=> (trim($listViewField[1])>0)?trim($listViewField[1]):10000
										);
				} else {
					$additionalFieldsAccepted = true;
					$listViewAdditionalFields = array();
					$listViewAdditionalFieldsTmp = explode("|",$listViewField[0]);
					if(count($listViewAdditionalFieldsTmp)>1){
						foreach($listViewAdditionalFieldsTmp AS $listViewAdditionalField){							
							if(in_array(trim($listViewAdditionalField),$this->getAllowedFields())){
								$listViewAdditionalFields[] = trim($listViewAdditionalField);								
							} else {
								$additionalFieldsAccepted = false;
								break;
							}							
						}
					} else {
						$additionalFieldsAccepted = false;
					}
					if($additionalFieldsAccepted){
						
						$mainViewField = $listViewAdditionalFields[0];
						unset($listViewAdditionalFields[0]);
						$listViewFields[] = array(	
													"name" 			=> $mainViewField,
													"lenght" 		=> (trim($listViewField[1])>0)?trim($listViewField[1]):10000,
													"additional" 	=> $listViewAdditionalFields
											);
					}
				}
			}
		}
		if(!count($listViewFields)){
			$listViewFields[0] = array("name" => "name", "length" => 10000);
			$listViewFields[1] = array("name" => "email", "length" => 10000);
		}
		return $listViewFields;
	}
	
	/**
	 * Get first folder with addresses module	
	 *	
	 * @return	array	folder 	
	 */
	public function getFirstFolder() {
		
		global $BE_USER;
		
		$query = array(
			'SELECT' => '*',
			'FROM' => 'pages',
			'WHERE' => $BE_USER->getPagePermsClause(1)." AND deleted=0 AND doktype=254 AND module='mxaddress'",
			'LIMIT' => 1
		);
		$pages = $GLOBALS['TYPO3_DB']->exec_SELECT_queryArray($query);		
		return $pages->fetch_assoc();		
	}
	
	/**
	 * Get file info	
	 *
	 * @param \int $uid
	 * @return	array	file 	
	 */
	public function getFileInfo($filename){
		$seperatorIndex 	= strrpos ($filename, ".");
		$file['name'] 		= substr ($filename, 0, $seperatorIndex);
		$file['extension'] 	= strtolower(substr ($filename, ($seperatorIndex+1)));
		return $file;
	}
	
	/**
	 * copy to temp	
	 *
	 * @param \string $source
	 * @param \string $filename
	 * @return	array	file 	
	 */
	public function copyToTypo3Temp($source,$filename){
		$tempdir = $GLOBALS["_SERVER"]["DOCUMENT_ROOT"]."/typo3temp/moox_address";
		$filenameParts = explode(".",$filename);		
		$tempfile = $tempdir."/".md5($filenameParts[0]).".".$filenameParts[1];
		if(!is_dir($tempdir)){
			mkdir($tempdir);
		}
		if(file_exists($tempfile)){
			unlink($tempfile);
		}
		if(!file_exists($tempfile)){
			move_uploaded_file($source,$tempfile);
		} 
		return $tempfile;
	}
	
	/**
	 * set page or redirect to default/last folder
	 *
	 * @param \array $folders
	 * @return void
	 */
	public function redirectToFolder($folders = array()) {
		
		$id 		= \TYPO3\CMS\Core\Utility\GeneralUtility::_GET('id');
		$id			= ($id!="")?(int)$id:NULL;
		
		if(is_null($this->backendSession->get("id")) && (int)$this->settings['addressStartPid']>0){
			$this->setPage((int)$this->settings['addressStartPid']);
			$this->backendSession->store("id",$this->page);
			$this->initializeStorageSettings();
		} elseif(is_null($this->backendSession->get("id")) && (int)$this->settings['addressStartPid']<1) {			
			$this->setPage(0);
			$this->backendSession->store("id",$this->page);
			$this->initializeStorageSettings();
		} elseif(!is_null($id)){
			$this->setPage($id);
			$this->backendSession->store("id",$this->page);
			$this->initializeStorageSettings();
		} else {
			$this->setPage($this->backendSession->get("id"));
			$this->backendSession->store("id",$this->page);
			$this->initializeStorageSettings();
		}
				
		if($this->page>0){
			$isAddressFolder = false;
			foreach($folders AS $addressFolder){
				if($addressFolder['uid']==$this->page){
					$isAddressFolder = true;
					break;
				}
			}
			if(!$isAddressFolder){
				$this->backendSession->delete("id");
				\TYPO3\CMS\Core\Utility\HttpUtility::redirect($_SERVER['REQUEST_URI']);				
			}
		}
	}
	
	/**
	 * process filter
	 *
	 * @param \array $filter
	 * @return \array $filter
	 */
	public function processFilter($filter = array()) {
		
		if($this->backendSession->get("filter")){
			$filter_tmp = unserialize($this->backendSession->get("filter"));
			if(isset($filter['mailing'])){
				$filter_tmp['mailing'] = $filter['mailing'];
			}
			if(isset($filter['state'])){
				$filter_tmp['state'] = $filter['state'];
			}
			if(isset($filter['perPage'])){
				$filter_tmp['perPage'] = $filter['perPage'];
			}
			if(isset($filter['sortField'])){
				$filter_tmp['sortField'] = $filter['sortField'];
			}
			if(isset($filter['sortDirection'])){
				$filter_tmp['sortDirection'] = $filter['sortDirection'];
			}
			if(isset($filter['query'])){
				$filter_tmp['query'] = $filter['query'];
			}
			$filter = $filter_tmp;			
		} else {			
			$filter['mailing'] 	= 0;			
			$filter['state'] 	= 0;			
			if($this->settings['itemsPerPage']>0){
				$filter['perPage'] 	= $this->settings['itemsPerPage'];
			} else {
				$filter['perPage'] 	= 25;
			}			
			if(in_array($this->settings['defaultSortField'],$this->getAllowedFields())){
				$filter['sortField'] = $this->settings['defaultSortField'];
			} else {
				$filter['sortField'] = "name";
			}							
			if(in_array($this->settings['defaultSortDirection'],array("ASC","DESC"))){
				$filter['sortDirection'] = $this->settings['defaultSortDirection'];
			} else {
				$filter['sortDirection'] = "ASC";
			}						
		}
		$this->backendSession->store("filter",serialize($filter));
		return $filter;
	}

	/**
	 * Returns page
	 *
	 * @return integer
	 */
	public function getPage() {
		return $this->page;
	}

	/**
	 * Set page
	 *
	 * @param integer $page page
	 * @return void
	 */
	public function setPage($page) {
		$this->page = $page;
	}
	
	/**
	 * Returns ext conf
	 *
	 * @return array
	 */
	public function getExtConf() {
		return $this->extConf;
	}

	/**
	 * Set ext conf
	 *
	 * @param array $extConf ext conf
	 * @return void
	 */
	public function setExtConf($extConf) {
		$this->extConf = $extConf;
	}

	/**
	 * Returns mailer active
	 *
	 * @return boolean
	 */
	public function getMailerActive() {
		return $this->mailerActive;
	}

	/**
	 * Set mailer active
	 *
	 * @param boolean $mailerActive mailer active
	 * @return void
	 */
	public function setMailerActive($mailerActive) {
		$this->mailerActive = $mailerActive;
	}
	
	/**
	 * Returns allowed fields
	 *
	 * @return array
	 */
	public function getAllowedFields() {
		return $this->allowedFields;
	}

	/**
	 * Set allowed fields
	 *
	 * @param array $allowedFields allowed fields
	 * @return void
	 */
	public function setAllowedFields($allowedFields) {
		$this->allowedFields = $allowedFields;
	}
}
?>