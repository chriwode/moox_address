<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}	

$actionList = "register,confirmRegistration,unregister,error";

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'DCNGmbH.'. $_EXTKEY,
	'Pi1',
	array(
		'Pi1' => $actionList,
	),
	// non-cacheable actions
	array(
		'Pi1' => $actionList,	
	)
);

// Hook backend preview of pi1
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem'][$_EXTKEY] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY).'Classes/Hooks/PageLayoutViewDrawItem.php:TxMooxAddressPageLayoutViewDrawItem';
?>