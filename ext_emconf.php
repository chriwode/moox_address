<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "moox_address".
 *
 * Auto generated 22-10-2015 09:24
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
    'title' => 'MOOX Address Management',
    'description' => 'Manage adresses and mail recipients for moox_mailer. With an easy-to-use CSV-Importer.',
    'category' => 'module',
    'author' => 'MOOX Team',
    'author_email' => 'moox@dcn.de',
    'author_company' => 'DCN GmbH',
    'state' => 'beta',
    'uploadfolder' => true,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'version' => '0.9.2',
    'constraints' => array(
        'depends' => array(
            'typo3' => '6.2.0-6.2.99',
            'vhs' => '2.4.0-2.4.99',
        ),
        'conflicts' => array(),
        'suggests' => array(),
    ),
    'clearcacheonload' => true,
);
