var $j = jQuery.noConflict();

$j(document).ready(function() {	
	
	$j("#selected-folder").click(function(){
		if ( $j("#folder-list").is(":visible") ) {
			$j( "#folder-expander" ).removeClass( "t3-icon-move-up t3-icon-move-down" );			
			$j( "#folder-expander" ).addClass( "t3-icon-move-down" );
		} else { 
			$j( "#folder-expander" ).removeClass( "t3-icon-move-up t3-icon-move-down" );
			$j( "#folder-expander" ).addClass( "t3-icon-move-up" );
		}		
		$j("#folder-list").toggle("fast");
		
	});
	
	$j("#folder-list-close").click(function(){
		$j("#folder-list").hide("fast");
	});
	
	$j("#filter-query").on("click", function () {
	   $j(this).select();
	});
	
	$j("#filter-mailing").on("change", function () {
	  $j("#filterForm").submit();
	});
	
	$j("#filter-state").on("change", function () {
	  $j("#filterForm").submit();
	});
	
	$j("#filter-per-page").on("change", function () {
	  $j("#filterForm").submit();
	});
	
	$j(this).ajaxStart(function(){         
		  $j("body").append("<div id='tx-moox-address-admin-overlay'><img src='/typo3conf/ext/moox_mailer/Resources/Public/Images/ajax-loader.gif' /></div>");
    });
	
	$j(this).ajaxStop(function(){
          $j("#tx-moox-address-admin-overlay").remove();
    });	
		
	$j(".address-details").on("click", function () {
		$j(this).hide("fast");
	});
});

function toggleDetails(id){
	$j("#folder-list").hide("fast");
	if ( $j("#address-details-" + id).is(":visible") ) {
		visible = true;
	} else { 
		visible = false;
	}	
	$j(".address-details").hide("fast");
	$j(".address-details-expander-reset").removeClass( "t3-icon-move-up t3-icon-move-down" );			
	$j(".address-details-expander-reset").addClass( "t3-icon-move-down" );
	if (visible) {		
		$j("#address-details-" + id).hide("fast");		
		$j("#address-details-" + id + "-expander").removeClass( "t3-icon-move-up t3-icon-move-down" );			
		$j("#address-details-" + id + "-expander").addClass( "t3-icon-move-down" );
	} else { 
		$j("#address-details-" + id).show("fast");
		$j("#address-details-" + id + "-expander").removeClass( "t3-icon-move-up t3-icon-move-down" );			
		$j("#address-details-" + id + "-expander").addClass( "t3-icon-move-up" );		
	}	
}